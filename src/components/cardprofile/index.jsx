import React from "react";
import { useNavigate } from "react-router-dom";
import Profilebg from "../../assets/img/Profilebg.png";
import Profilepict from "../../assets/img/Profilepict.png";

const CardP = () => {
  const navigate = useNavigate();
  const logout = () => {
    localStorage.clear();
    navigate("/login");
  };

  return (
    <div className=" flex flex-wrap items-center justify-center">
      <div className="container max-w-lg bg-white rounded dark:bg-gray-800 shadow-lg transform duration-200 easy-in-out m-12">
        <div className="h-2/4 sm:h-64 overflow-hidden">
          <img
            className="w-full rounded-t"
            src={Profilebg}
            alt="Photo profile"
          />
        </div>
        <div className="flex justify-start px-5 -mt-12 mb-5">
          <span clspanss="block relative h-32 w-32">
            <img
              alt="Photo profile"
              src={Profilepict}
              className="mx-auto object-cover rounded-full h-24 w-24 bg-white p-1"
            />
          </span>
        </div>
        <div className="">
          <div className="px-7 mb-8">
            <h2 className="text-3xl font-bold dark:text-gray-300">
              Nama Pengguna
            </h2>
            <p className=" mt-2 dark:text-gray-400">Email : api</p>
            <p className=" mt-2 dark:text-gray-400">Phone Number : api</p>
            <button
              className="px-4 py-2 cursor-pointer bg-blue-700 max-w-min mx-auto mt-8 rounded-lg text-gray-300 hover:bg-[#C7D7FC] hover:text-gray-100 dark:bg-gray-700 dark:text-gray-300 dark:hover:bg-gray-600 dark:hover:text-gray-200"
              onClick={logout}
            >
              LogOut
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CardP;
