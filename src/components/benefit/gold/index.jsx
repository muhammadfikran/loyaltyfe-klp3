import React from "react";

export const Gold = () => {
  return (
    <section class="block border border-gray-200 bg-white dark:bg-gray-900">
      <div class="py-8 px-4 mx-auto max-w-screen-xl sm:py-16 lg:px-6">
        <div class="max-w-screen-md mb-8 lg:mb-16">
          <h2 class="mb-4 text-4xl tracking-tight font-extrabold text-gray-900 dark:text-white">
            Silver
          </h2>
          <p class="text-gray-500 sm:text-xl dark:text-gray-400">
            Kamu adalah loyalty Silver kami, kamu mendapatkan keuntungan
            discount 15% saat pembelian product kami.
          </p>
        </div>
      </div>
    </section>
  );
};

export default Gold;
