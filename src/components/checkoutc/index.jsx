import React from 'react'
import { Card } from 'antd';

const CheckoutC = () => {
  return (
    <div>
        <div className="mx-auto container ">
          <div className=" mx-60 grid grid-cols-3 items-center lg:justify-between justify-center">
            {/* Card 1 */}
            <div className="site-card-border-less-wrapper mr">
                    <Card
                    title="Card title"
                    bordered={false}
                    style={{
                        width: 300,
                    }}
                    >
                    </Card>
                </div>
                <div className="site-card-border-less-wrapper mr">
                    <Card
                    title="Card title"
                    bordered={false}
                    style={{
                        width: 300,
                    }}
                    >
                    </Card>
                </div>
            </div>
        </div>
    </div>
  )
}

export default CheckoutC