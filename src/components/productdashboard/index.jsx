import React from "react";
import { useNavigate } from "react-router-dom";
import burger2 from "../../assets/img/burger2.jpg";

const ProductDashboard = ({ productName, productPrice, onClick }) => {
  const navigate = useNavigate();

  return (
    <div>
      <div className="bg-gray-100 py-8">
        <div className="mx-auto container ">
          <div className=" mx-1 flex flex-wrap items-center lg:justify-between justify-center ">
            <div className="w-full mt-5 max-w-sm bg-white rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
              <a href="/product">
                <img
                  className="p-8 rounded-t-lg"
                  src={burger2}
                  alt="product image"
                />
              </a>
              <div className="px-5 pb-5">
                <h5 className="text-xl font-semibold tracking-tight text-gray-900 dark:text-white">
                  Nama Product : {productName}
                </h5>

                <div className="flex items-center justify-between">
                  <span className="text-3xl font-bold text-gray-900 dark:text-white">
                    Rp. {productPrice}
                  </span>
                  <button
                    onClick={() => navigate(`/product/${onClick}`)}
                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Buy Now
                  </button>
                </div>
              </div>
            </div>
            {/* Card */}
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDashboard;
