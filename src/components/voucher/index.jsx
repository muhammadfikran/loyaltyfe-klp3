import React from "react";
import voc from "../../assets/img/voucher.png";

export const ManyVoucher = ({
  voucherName,
  voucherPoint,
  voucherCode,
  merchantName,
}) => {
  return (
    <div>
      <div className="bg-gray-100 py-8">
        <div className="mx-auto container ">
          <div className=" mx-1 flex flex-wrap items-center lg:justify-between justify-center">
            {/* Card 1 */}
            <div className="w-full max-w-sm bg-white rounded-lg shadow-md dark:bg-gray-800 dark:border-gray-700">
              <a href="/showvoucher">
                <img
                  className="p-8 rounded-t-lg"
                  src={voc}
                  alt="product image"
                />
              </a>
              <div className="px-5 pb-5">
                <a href="#">
                  <h5 className="text-xl font-semibold tracking-tight text-gray-900 dark:text-white">
                    Nama Voucher: {voucherName}
                  </h5>
                  <br />
                  <h5 className="text-xl font-semibold tracking-tight text-gray-900 dark:text-white">
                    Kode Voucher : {voucherCode}
                  </h5>
                </a>
                <div className="flex items-center justify-between">
                  <span className="text-3xl font-bold text-gray-900 dark:text-white">
                    {voucherPoint} Point
                  </span>
                  <a
                    href="#"
                    className="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800"
                  >
                    Reedem
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ManyVoucher;
