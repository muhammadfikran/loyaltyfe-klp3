import React from "react";

const Pointandbalance = () => {
  return (
    <div>
      <div className="mx-auto container ">
        <br></br>
        <div className="flex">
          {/* Balance */}
          <br></br>
          <a className="block w-72 h-40 px-10 bg-white border border-gray-200 rounded-lg shadow-md  dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700">
            <br></br>
            <h5 className="mb-2 mr-20 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
              Balance
            </h5>
            <br></br>
            <p className="font-semibold text-gray-700 dark:text-gray-400">
              Rp.400000
            </p>
            <br></br>
          </a>
          {/* Point */}
          <br></br>
          <div className="ml-20">
            <a
              href="/benefit"
              className="block w-72 h-40 px-10 bg-white border border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
            >
              <br></br>
              <h5 className="mb-2 mr-20 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                Point{" "}
                <span>
                  <br />
                  Silver
                </span>
              </h5>
              <br></br>
              <p className="font-semibold text-gray-700 dark:text-gray-400">
                0
              </p>
              <br></br>
            </a>
          </div>
          {/* Voucher */}
          <br></br>
          <div className="ml-20">
            <a
              href="/myvoucher"
              className="block w-72 h-40 px-10 bg-white border border-gray-200 rounded-lg shadow-md hover:bg-gray-100 dark:bg-gray-800 dark:border-gray-700 dark:hover:bg-gray-700"
            >
              <br></br>
              <h5 className="mb-2 mr-20 text-2xl font-bold tracking-tight text-gray-900 dark:text-white">
                Voucher{" "}
              </h5>
              <br></br>
              <p className="font-semibold text-gray-700 dark:text-gray-400">
                0
              </p>
              <br></br>
            </a>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Pointandbalance;
