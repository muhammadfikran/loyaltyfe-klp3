import React from "react";
import Clandingpict from "../../assets/img/Clandingpict.png";

const Clanding = () => {
  return (
    <div>
      <div className="py-16 bg-white">
        <div className="container m-auto px-6 text-gray-600 md:px-12 xl:px-6">
          <div className="space-y-6 md:space-y-0 md:flex md:gap-6 lg:items-center lg:gap-12">
            <div className="md:5/12 lg:w-5/12">
              <img
                src={Clandingpict}
                alt="image"
                loading="lazy"
                width=""
                height=""
              />
            </div>
            <div className=" md:7/12 lg:w-6/12">
              <h2 className="text-2xl text-gray-900 font-bold md:text-3xl">
                Loyalty Studio adalah sebuah web application yang memudahkan
                anda untuk mendapatkan keuntungan melalui Point
              </h2>
              <p className="mt-6 text-gray-600">
                Dengan website ini kalian dapat melakukan transaksi pembelian
                product untuk mendapatkan loyalty point yang nantinya dapat di
                tukarkan untuk voucher-voucher discount yang tersedia.
              </p>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Clanding;
