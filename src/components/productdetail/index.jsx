import React, { useState } from "react";
import burger from "../../assets/img/burgerpolos.png";

const ProductDetail = () => {
  const [rotate, setRotate] = useState(false);
  const [count, setCount] = useState(0);

  const addCount = () => {
    setCount((prev) => prev + 1);
  };

  const minusCount = () => {
    if (count > 0) {
      setCount((prev) => prev - 1);
    }
  };

  return (
    <div className="2xl:container 2xl:mx-auto lg:py-16 lg:px-20 md:py-12 md:px-6 py-9 px-4 ">
      <div className="flex justify-center items-center lg:flex-row flex-col gap-8">
        <div className="  w-full sm:w-96 md:w-8/12 lg:w-6/12 items-center">
          <h2 className="font-semibold lg:text-4xl text-3xl lg:leading-9 leading-7 text-gray-800 mt-4">
            Burger
          </h2>

          <p className=" font-normal text-base leading-6 text-gray-600 mt-7">
            Burger Indomaret rasanya enak dan lezat
          </p>
          <p className=" font-semibold lg:text-2xl text-xl lg:leading-6 leading-5 mt-6 ">
            Rp.20
          </p>
          <div className="mb-6">
            <br></br>
            <input
              type="text"
              className="block w-20 px-2 py-0 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
              id="exampleFormControlInput2"
              placeholder="0"
            />
          </div>

          <button className="focus:outline-none focus:ring-2 hover:bg-black focus:ring-offset-2 focus:ring-gray-800 font-medium text-base leading-4 text-white bg-gray-800 w-full py-5 lg:mt-12 mt-6">
            Buy
          </button>
        </div>

        <div className=" w-full sm:w-96 md:w-11/12  lg:w-6/12 flex lg:flex-row flex-col lg:gap-8 sm:gap-6 gap-4">
          <div className=" w-full lg:w-full bg-gray-100 flex justify-center items-center">
            <img src={burger} alt="Product Previw" />
          </div>
        </div>
      </div>
    </div>
  );
};

export default ProductDetail;
