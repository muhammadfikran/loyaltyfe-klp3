import React from "react";
import burger from "../../assets/img/burgerpolos.png";

const Banner = () => {
  return (
    <>
      <div>
        <div className="lg:mt-10">
          <div className="lg:max-w-[2000px] md:max-w-[744px] max-w-[500px] h-[380px]  bg-[#C7D7FC] px-4 py-12">
            <div className="md:flex justify-center gap-8 items-center">
              <div>
                <img
                  src={burger}
                  width="300"
                  alt="hair_care"
                  className="lg:block md:hidden block"
                />
              </div>
              <div>
                <p className="lg:text-4xl md:text-3xl text-3xl font-semibold md:text-left text-center">
                  Burger Slay
                </p>
                <p className="text-base max-w-[624px] w-full mt-6 md:text-left text-center">
                  “Spoil” your hair with our new hair restoration mask. Manage
                  common hair related problems such as hair fall, dryness,
                  dandruff, frizzy hair, thinning hair with our new product.
                </p>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default Banner;
