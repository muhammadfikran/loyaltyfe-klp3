import React from "react";
import { FaFacebookSquare, FaInstagram, FaTwitterSquare } from "react-icons/fa";
const Footer = () => {
  return (
    <div className="bg-[#C7D7FC] flex mt-20  mx-auto py-16 px-10 text-gray-300">
      <div>
        <h1 className="w-full text-3xl font-bold text-black ">
          Loyalty Studio.
        </h1>
        <p className="py-4 text-black ">
          <span>
            <br />
          </span>
          Dengan website ini kalian dapat melakukan transaksi pembelian product
          untuk mendapatkan loyalty point yang nantinya dapat di tukarkan untuk
          voucher-voucher discount yang tersedia.
        </p>
        <div className="text-black flex  md:w-[75%] my-6">
          <FaFacebookSquare size={30} />
          <FaInstagram size={30} />
          <FaTwitterSquare size={30} />
        </div>
      </div>
      <div className="text-black ml-5  lg:col-span-2 flex justify-between mt-6">
        <div>
          <h6 className="font-bold text-base text-black ">Kontak Kami</h6>
          <ul>
            <li className="py-2 text-sm"> PT. Indivara</li>
            <li className="py-2 text-sm">Jl. Kelapa Gading</li>
            <li className="py-2 text-sm">Jakarta </li>
            <li className="py-2 text-sm">Indonesia</li>
          </ul>
        </div>
      </div>
    </div>
  );
};

export default Footer;
