import axios from "axios";

const api = axios.create({
  baseURL: "http://localhost:8080",
});

// api.interceptors.request.use((config) => {
//   const auth = localStorage.getItem("token");
//   // const token = JSON.parse(auth);
//   console.log(auth);
//   config.headers["Authorization"] = `Bearer ${auth}`;
// });

export default api;
