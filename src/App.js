import { BrowserRouter, Route, Routes } from "react-router-dom";
import "./App.css";
import {
  Login,
  Register,
  History,
  Landing,
  AboutUs,
  Profile,
  Dashboard,
  Product,
  Benefits,
  Voucher,
  AnyVoucher,
  Checkoutc,
} from "@pages";
import { MyVoucher } from "./pages";
import ProtectedRoutes from "./protectedroute";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path="/" element={<Landing />} />
        <Route path="/login" element={<Login />} />
        <Route path="/register" element={<Register />} />

        <Route element={<ProtectedRoutes />}>
          <Route path="/history" element={<History />} />
          <Route path="/aboutus" element={<AboutUs />} />
          <Route path="/profile" element={<Profile />} />
          <Route path="/dashboard" element={<Dashboard />} />
          <Route path="/product" element={<Product />} />
          <Route path="/benefit" element={<Benefits />} />
          <Route path="/showvoucher" element={<Voucher />} />
          <Route path="/voucher" element={<AnyVoucher />} />
          <Route path="/checkout" element={<Checkoutc />} />
          <Route path="/myvoucher" element={<MyVoucher />} />
        </Route>
      </Routes>
    </BrowserRouter>
  );
}

export default App;
