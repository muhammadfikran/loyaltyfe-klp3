import React, { useEffect, useState } from "react";
import register from "../../assets/img/register.png";
import logo from "../../assets/img/logo.jpeg";
import { useNavigate } from "react-router-dom";
import axios from "axios";

const Register = () => {
  const navigate = useNavigate();
  const [userName, setuserName] = useState("");
  const [userEmail, setuserEmail] = useState("");
  const [userPassword, setuserPassword] = useState("");
  const [userPhonenum, setuserPhoneNum] = useState("");

  const [messagePass, setpassMessage] = useState("");
  const [messageEmail, setemailMessage] = useState("");

  useEffect(() => {
    const auth = localStorage.getItem("token");
    if (auth) {
      navigate("/dashboard");
    }
  });

  const Regis = (e) => {
    // const passValid = /^(?=.\d)(?=.[a-z])(?=.*[A-Z])$/;
    // const emailValid = /^[a-zA-Z0-9._]+@[a-z]+\.[a-z]{2,6}$/;

    e.preventDefault();
    axios
      .post("http://localhost:8080/auth/register", {
        username: userName,
        email: userEmail,
        password: userPassword,
        phone: userPhonenum,
      })
      .then(function (response) {
        console.log(response);
        localStorage.setItem("user", userName);
        localStorage.setItem("userEmail", userEmail);

        navigate("/login");
      })
      .catch(function (error) {
        console.log(error);
      });
  };
  return (
    <div>
      <section className="h-screen">
        <div className="px-6 h-full text-gray-800">
          <div className="flex xl:justify-center lg:justify-between justify-center items-center flex-wrap h-full g-6">
            <div className="grow-0 shrink-1 md:shrink-0 basis-auto xl:w-6/12 lg:w-6/12 md:w-9/12 mb-12 md:mb-0">
              <img src={register} className="w-full" alt="Sample image" />
            </div>
            <div className="xl:ml-20 xl:w-5/12 lg:w-5/12 md:w-8/12 mb-12 md:mb-0">
              <form>
                <div className="flex items-center before:flex-1 before:border-t before:border-gray-300 before:mt-0.5 after:flex-1 after:border-t after:border-gray-300 after:mt-0.5">
                  <img src={logo} className="text-center h-1/3 w-1/3"></img>
                </div>
                {/* Name input */}
                <div className="mb-6">
                  <input
                    type="text"
                    className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    id="inputForm"
                    placeholder="Nama"
                    onChange={(e) => setuserName(e.target.value)}
                  />
                </div>
                {/* Number input */}
                <div className="mb-6">
                  <input
                    type="tel"
                    pattern="[0-9]{4}-[0-9]{4}-[0-9]{4}"
                    className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none "
                    id="inputForm"
                    placeholder="0813-1870-5575"
                    onChange={(e) => setuserPhoneNum(e.target.value)}
                    required
                  />
                </div>
                {/* Email input */}
                <div className="mb-6">
                  <input
                    type="email"
                    className="form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none invalid:border-red-500 invalid:text-red-500 invalid:focus:border-red-500"
                    placeholder="Email address"
                    onChange={(e) => setuserEmail(e.target.value)}
                  />
                  <p>{messageEmail}</p>
                </div>
                {/* Password input */}
                <div className="mb-6">
                  <input
                    type="password"
                    className="  form-control block w-full px-4 py-2 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    id="inputForm"
                    placeholder="Password"
                    onChange={(e) => setuserPassword(e.target.value)}
                  />
                  <p>{messagePass}</p>
                </div>
                <div className="items-center lg:text-center">
                  <button
                    type="button"
                    className=" inline-block px-7 py-3 bg-blue-600 text-white font-medium text-sm leading-snug uppercase rounded-3xl shadow-md hover:bg-blue-700 hover:shadow-lg focus:bg-blue-700 focus:shadow-lg focus:outline-none focus:ring-0 active:bg-blue-800 active:shadow-lg transition duration-150 ease-in-out"
                    onClick={Regis}
                  >
                    Register
                  </button>
                </div>
              </form>
            </div>
          </div>
        </div>
      </section>
    </div>
  );
};

export default Register;
