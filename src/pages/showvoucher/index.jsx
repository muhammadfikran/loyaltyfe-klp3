import React, { useEffect } from "react";
import { DetailVoucher, NavbarDashboard } from "../../components";

const Voucher = () => {
  return (
    <div>
      <NavbarDashboard />
      <DetailVoucher />
    </div>
  );
};

export default Voucher;
