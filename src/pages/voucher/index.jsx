import React, { useEffect, useState } from "react";
import { NavbarDashboard, ManyVoucher } from "../../components";
import api from "../../service/api";

const AnyVoucher = () => {
  const auth = localStorage.getItem("token");
  const token = JSON.parse(auth);

  const [voucher, setVoucher] = useState([]);
  const fetchVoucher = async () => {
    try {
      const url = "/voucher";
      const response = await api.get(url, {
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const payload = [...response.data.data];
      console.log(payload);
      setVoucher(payload);
    } catch (error) {
      alert(error);
    }
  };

  useEffect(() => {
    fetchVoucher();
  }, []);

  return (
    <div>
      <NavbarDashboard />
      <p className="text-center text-bold pb-10 text-5xl">Vouchers</p>
      <div className="grid grid-cols-4 gap-10 mt-5 m-5">
        {voucher.map((item) => (
          <ManyVoucher
            voucherName={item.vouchername}
            voucherPoint={item.voucherpoint}
            voucherCode={item.code}
          />
        ))}
      </div>
    </div>
  );
};

export default AnyVoucher;
