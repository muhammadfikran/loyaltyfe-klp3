import React from "react";
import { Gold, NavbarDashboard } from "../../components";

const Benefits = () => {
  return (
    <div>
      <NavbarDashboard/>
      <Gold />
    </div>
  );
};

export default Benefits;
