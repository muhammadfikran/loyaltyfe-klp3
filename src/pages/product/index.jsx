import React from "react";
import { ProductDetail, NavbarDashboard } from "../../components";

const Product = () => {
  return (
    <div>
      <NavbarDashboard />
      <ProductDetail />
    </div>
  );
};

export default Product;