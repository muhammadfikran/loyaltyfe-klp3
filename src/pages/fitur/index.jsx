import React from "react";
import { CardF, NavbarLanding } from "../../components";

const Fitur = () => {
  return (
    <div>
      <NavbarLanding />
      <CardF />
    </div>
  );
};

export default Fitur;
