import React, { useState } from "react";
import { Banner, NavbarDashboard, Footer } from "@components";
import { Pointandbalance, ProductDashboard } from "../../components";
import { useEffect } from "react";
import api from "../../service/api";

const Dashboard = () => {
  const auth = localStorage.getItem("token");
  const token = JSON.parse(auth);

  const [products, setProducts] = useState([]);
  const fetchProduct = async () => {
    try {
      const url = "/product";
      const response = await api.get(url, {
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const payload = [...response.data.data];
      console.log(payload);
      setProducts(payload);
    } catch (error) {
      alert(error);
    }
  };

  const [user, setUser] = useState([]);

  const fetchUser = async () => {
    try {
      const url = `/user/`;
      const response = await api.get(url, {
        headers: {
          authorization: `Bearer ${token}`,
        },
      });
      const payload = [...response.data.data];
      // console.log(payload);
      setUser(payload);
    } catch (error) {
      alert(error);
    }
  };

  useEffect(() => {
    fetchProduct();
  }, []);

  useEffect(() => {
    fetchUser();
  }, []);

  return (
    <div>
      <NavbarDashboard />
      <h5 className="ml-8 mt-5 font-semibold text-4xl">
        Welcome {user.username}
      </h5>
      <Pointandbalance />
      <Banner />
      <p className="text-center text-5xl">Products</p>
      <div className="grid grid-cols-4 gap-10 mt-5 m-5">
        {products.map((item) => (
          <ProductDashboard
            productName={item.productname}
            productPrice={item.price}
            description={item.description}
            // onClick={item.id}
          />
        ))}
      </div>
      <Footer />
    </div>
  );
};

export default Dashboard;
