import React from "react";
import { CardP, NavbarDashboard, Footer } from "../../components";

const Profile = () => {
  return (
    <div>
      <NavbarDashboard />
      <CardP />
    </div>
  );
};

export default Profile;
