import React from "react";
import { Card } from "antd";
import { NavbarDashboard } from "../../components";

const CheckoutC = () => {
  return (
    <div>
      <NavbarDashboard />
      <div className="mx-auto container ">
        <div className=" flex items-center lg:justify-between justify-center">
          {/* Card 1 */}
          <div className="site-card-border-less-wrapper mr">
            <Card
              title="Keranjang"
              bordered={false}
              style={{
                width: 1050,
              }}
            >
              <p>Burger Slay</p>
              <br></br>
              <p className="mr-10">
                Burger enak banget sumpah suwer gak boong langsung aja di beli
                ges
              </p>
              <p>
                Rp.25000
                <div className="mb-6">
                  <br></br>
                  <input
                    type="text"
                    className="block w-20 px-2 py-0 text-xl font-normal text-gray-700 bg-white bg-clip-padding border border-solid border-gray-300 rounded transition ease-in-out m-0 focus:text-gray-700 focus:bg-white focus:border-blue-600 focus:outline-none"
                    id="exampleFormControlInput2"
                    placeholder="0"
                  />
                </div>
              </p>
            </Card>
          </div>
          <div className="site-card-border-less-wrapper mr">
            <br></br>
            <br></br>
            <Card
              title="Ringkasan Belanja"
              bordered={false}
              style={{
                width: 400,
              }}
            >
              <p>
                Sub Total (jumlah : barang api) <span>Rp.25000</span>{" "}
              </p>
              <br></br>
              <hr className="border-2"></hr>
              <br></br>
              <p>
                Total Harga : <span>Rp.25000</span>
              </p>
            </Card>
            <br></br>
            <button
              type="button"
              class="text-gray-900 bg-gradient-to-r from-red-200 via-red-300 to-yellow-200 hover:bg-gradient-to-bl focus:ring-4 focus:outline-none focus:ring-red-100 dark:focus:ring-red-400 font-medium rounded-lg text-sm px-5 py-2.5 text-center mr-2 mb-2"
            >
              Check Out
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default CheckoutC;
