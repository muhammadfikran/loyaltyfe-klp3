import React, { useEffect } from "react";
import { Clanding, NavbarLanding } from "@components";
import { Navigate } from "react-router-dom";

const Landing = () => {
  // useEffect(() => {
  //   const auth = localStorage.getItem("token");
  //   if (auth) {
  //     Navigate("/dashboard");
  //   }
  // });
  return (
    <div>
      <NavbarLanding />
      <Clanding />
    </div>
  );
};

export default Landing;
