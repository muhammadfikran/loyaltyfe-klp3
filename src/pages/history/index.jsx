import React from "react";
import { Cards, NavbarDashboard } from "../../components";

const History = () => {
  return (
    <div>
      <NavbarDashboard />
      <Cards />
    </div>
  );
};

export default History;
