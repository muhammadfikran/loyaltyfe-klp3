import React from "react";
import { Card } from "antd";
import { NavbarDashboard } from "../../components";

const MyVoucher = () => {
  return (
    <div>
      <NavbarDashboard />
      <div className="mx-auto container ">
        <div className=" flex items-center lg:justify-between justify-center">
          {/* Card 1 */}
          <div className="site-card-border-less-wrapper mr">
            <br></br>
            <br></br>
            <Card
              title="Voucher"
              bordered={false}
              style={{
                width: 1280,
              }}
              className="bg-slate-300"
            >
              <p className="text-lg">Indomaret</p>
              <p>Voucher Diskon 25 Ribu</p>
              <br></br>
              <p className="mr-10">
                Burger enak banget sumpah suwer gak boong langsung aja di beli
                ges
              </p>
              <p className="font-semibold">
                100 Point
                <div className="mb-6">
                  <br></br>

                  <p className="font-bold text-lg">2T9AUV3YPV49 </p>
                </div>
              </p>
            </Card>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MyVoucher;
